import logging
import os.path
import subprocess
import json

from cuckoo.common.abstracts import Processing
from cuckoo.common.exceptions import CuckooProcessingError
from cuckoo.common.objects import File


log = logging.getLogger(__name__)


class Capa(Processing):

    def run(self):
        self.key = "capa"
        log.info("Starting CAPA Analysis")

        if self.task["category"] == "file":
            if not os.path.exists(self.file_path):
                raise CuckooProcessingError(
                    "Sample file doesn't exist: \"%s\"" % self.file_path
                )
        else:
            return

        self.capa = self.options.get("capa", "/usr/local/bin/capa")

        if not os.path.isfile(self.capa):
            raise CuckooProcessingError("Unable to locate capa binary")

        args = [
            self.capa,
            self.file_path,
            "-j",
        ]

        try:
            capa_json = json.loads(subprocess.check_output(args))
        except subprocess.CalledProcessError as e:
            raise CuckooProcessingError(
                "capa returned an error processing this sample: %s" % e)
        # write out the raw json results
        capa_path = os.path.join(self.analysis_path, "capa.json")
        with open(capa_path, "w") as out:
            out.write(json.dumps(capa_json))

        capa_results = {'rules': []}
        # reformat the results for display in cuckoo's static analysis section
        capa_results['md5'] = capa_json["meta"]["sample"]["md5"]
        capa_results['sha1'] = capa_json["meta"]["sample"]["sha1"]
        capa_results['sha256'] = capa_json["meta"]["sample"]["sha256"]
        capa_results['path'] = capa_json["meta"]["sample"]["path"]
        capa_results['timestamp'] = capa_json["meta"]["timestamp"]
        capa_results['version'] = capa_json["meta"]["version"]
        capa_results['format'] = capa_json["meta"]["analysis"]["format"]
        capa_results['extractor'] = capa_json["meta"]["analysis"]["extractor"]
        capa_results['base_address'] = hex(capa_json["meta"]["analysis"]["base_address"])
        capa_results['rule_sets'] = capa_json["meta"]["analysis"]["rules"]
        capa_results['function_count'] = len(capa_json["meta"]["analysis"]["feature_counts"]["functions"])

        functions = capa_json['total_feature_count'] = capa_json["meta"]["analysis"]["feature_counts"]["functions"]
        feature_count = 0
        for f in functions.keys():
            feature_count = feature_count + functions[f]

        capa_results['total_feature_count'] = capa_json["meta"]["analysis"]["feature_counts"]["file"] + feature_count

        for _, rule in capa_json["rules"].items():
            new_rule = {'rule_name': rule['meta']['name']}
            matches = list(rule["matches"].keys())
            new_rule['matches'] = [hex(int(address)) for address in matches]
            if "namespace" in rule['meta']:
                new_rule["namespace"] = rule['meta']['namespace']
            if "att&ck" in rule['meta']:
                new_rule["attack"] = [{"tactic": method.split("::")[0], "technique": method.split("::")[1]} for method
                                      in rule['meta']['att&ck']]
            if "mbc" in rule['meta']:
                new_rule["mbc"] = [{"objective": method.split("::")[0], "behavior": method.split("::")[1]} for method in
                                   rule['meta']['mbc']]
            capa_results['rules'].append(new_rule)

        # deduplicate the ATT&CK and MBC results for display
        attack = []
        mbc = []
        for rule in capa_results['rules']:
            if "attack" in rule:
                for a in rule["attack"]:
                    if a not in attack:
                        attack.append(a)
            if "mbc" in rule:
                for a in rule["mbc"]:
                    if a not in mbc:
                        mbc.append(a)

        capa_results['attack'] = attack
        capa_results['mbc'] = mbc

        return capa_results
