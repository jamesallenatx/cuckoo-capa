# cuckoo-capa

This module is provides [capa](https://github.com/fireeye/capa) analysis to Cuckoo Sandbox. 

This project was completed in partial fulfillment of CSEC.759.03 Advanced Malware Analysis Spring 2021

## Compatibility

This plugin is meant for use with Cuckoo Sandbox, Fireeye CAPA, and a 64-bit linux kernel.

> This project was developed with 64-bit Ubuntu 20.04 running Cuckoo Sandbox 2.0.7

## Requirements

Before this module can be used on a project, you must ensure that the following pre-requisites are fulfilled:

| Name                                                                         | Version |
| ---------------------------------------------------------------------------- | ------- |
| [Cuckoo Sandbox](https://github.com/cuckoosandbox/cuckoo/releases/tag/2.0.7) | 2.0.7   |
| [FireEye CAPA](https://github.com/fireeye/capa/releases/tag/v1.6.0)          | 1.6.0   |


> Fireeye's CAPA binary requires a 64-bit linux kernel. Attempting to run the standalone binary version of CAPA on a 32-bit kernel will result in an error.

## Installation
Installation requires modification of Cuckoo Sandbox's `config.py`  and `index.html` files in the Cuckoo Sandbox installation directory.  Two installation procedures have been provided. The [Standard Installation](#standar-installation) section is the easier of the two methods and automates the changes required to use the plugin. the [Alternate Installation](#alternate-installation) instructions details the modifications that need to be made to install the plugin.

### Standard Installation
```bash
# Open a terminal window and navigate to this repo's root directory
# Run the install.sh script using sudo
$ sudo install.sh
```

### Alternate Installation
1. Install Fireeye CAPA using pip by running:
   ```bash
    wget https://github.com/fireeye/capa/releases/download/v1.6.0/capa-v1.6.0-linux.zip
    unzip capa-v1.6.0-linux.zip
    mv ./capa /usr/local/bin/capa
   ```
2. Open `cuckoo/common/config.py` in your text editor
3. Insert the following lines after the `irma` section at line 661
   ```python
            ...
            "irma": {
                "enabled": Boolean(False),
                "timeout": Int(60),
                "scan": Boolean(False),
                "force": Boolean(False),
                "url": String(),
                "probes": String(required=False),
            },
            "capa": {
                "enabled": Boolean(True),
            },
            ...
   ```
4. Open `.cuckoo/conf/processing.conf` in your text editor
5. Insert the following lines after the `irma` configuration at line 180
   ```toml
   # Fireeye CAPA processing
   [capa]
   enabled = yes
   ```
6. Copy `web/templates/_capa.html` and `web/templates/index.html` to `cuckoo/web/templates/analysis/pages/static`
7. copy `capa.py` to `cuckoo/processing/capa.py`
8. start Cuckoo Sandbox as normal

## Usage
After installation use Cuckoo Sandbox to submit malware samples. CAPA output will be included in the `report.json` and displayed on the static analysis web interface for the relevant sample.

## Inputs

## Outputs
The module creates a `capa.json` file containing the raw CAPA output for further inspection, processing, or importing to other tools like IDA Pro.

After a malware sample is submitted to Cuckoo Sandbox and processing occurs CAPA output can be found by navigating to the sample results and looking at the static results in the web interface. Clicking the CAPA tab will present the user with all capabilities identified along with address offsets. Ouput will include both ATT&CK Tactics and Techniques will be displayed and Identities from the [Malware Behavior Catalog](https://github.com/MBCProject/mbc-markdown)

No output.
