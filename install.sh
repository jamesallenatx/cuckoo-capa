set -e pipefail
if [ -f /usr/local/bin/capa ] 
then
    echo "capa is present - skipping install..."
else
    echo "install fireeye capa..."
    mkdir -p  ./Downloads
    cd ./Downloads
    wget https://github.com/fireeye/capa/releases/download/v1.6.0/capa-v1.6.0-linux.zip
    unzip capa-v1.6.0-linux.zip
    mv ./capa /usr/local/bin/capa
    cd -
    rm -rf Downloads
fi 


if [ -d /usr/local/lib/python2.7/dist-packages/cuckoo/processing ]
then
    echo "adding capa.py processing module..."
    cp ./capa.py /usr/local/lib/python2.7/dist-packages/cuckoo/processing/capa.py
else
    echo "Missing Cuckoo Sandbox process directory at /usr/local/lib/python2.7/dist-packages/cuckoo/processing"
fi 

if [ -f /usr/local/lib/python2.7/dist-packages/cuckoo/common/config.py ]
then
    echo "replacing config.py in cuckoo/common/..."
    cp ./common/config.py /usr/local/lib/python2.7/dist-packages/cuckoo/common/config.py
else
    echo "Missing Cuckoo Sandbox config.py file at /usr/local/lib/python2.7/dist-packages/cuckoo/common/config.py"
fi 

if [ -d /usr/local/lib/python2.7/dist-packages/cuckoo/web/templates/analysis/pages/static ]
then
    echo "adding _capa.html to cuckoo/web/templates/analysis/pages/static..."
    cp ./web/templates/_capa.html /usr/local/lib/python2.7/dist-packages/cuckoo/web/templates/analysis/pages/static/_capa.html
else
    echo "Missing Cuckoo Sandbox web/templates/analysis/pages/static directory at /usr/local/lib/python2.7/dist-packages/cuckoo/web/templates/analysis/pages/static"
fi 

if [ -f /usr/local/lib/python2.7/dist-packages/cuckoo/web/templates/analysis/pages/static/index.html ]
then
    echo "replacing index.html in web/templates/analysis/pages/static..."
    cp ./web/templates/index.html /usr/local/lib/python2.7/dist-packages/cuckoo/web/templates/analysis/pages/static/index.html
else
    echo "Missing Cuckoo Sandbox static/index.html file at /usr/local/lib/python2.7/dist-packages/cuckoo/web/templates/analysis/pages/static/index.html"
fi 

echo "adding capa config to /home/$(logname)/.cuckoo/conf/processing.conf..."
sudo -u $(logname) cat << EOF >> "/home/$(logname)/.cuckoo/conf/processing.conf"
# Fireeye CAPA processing
[capa]
enabled = yes
EOF

